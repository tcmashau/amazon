package com.amazon.test;

import org.junit.Assert;
import org.junit.Test;
import java.util.List;
import java.util.Map;

/**
 * Created by caesarmashau on 2016/12/04.
 */
public class FileReaderTest {

    FileReader fileReader = new FileReader();

    @Test
    public void getHosts() throws Exception {
        Map<Host.Status, Map<Host.InstanceType, List<Host>>> host = fileReader.getReport();
        Assert.assertNotNull(host);
    }

    @Test
    public void getReport() throws Exception {
        Map<Host.Status, Map<Host.InstanceType, List<Host>>> report = fileReader.getReport();
        Assert.assertNotNull(report);
    }

    @Test
    public void printReport() throws Exception {
        fileReader.printReport();
    }

}
