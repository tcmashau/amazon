package com.amazon.test;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by caesarmashau on 2016/12/04.
 */
public class Host {



    public enum InstanceType {M1, M2, M3}
    public enum Status {Empty,FULL,MOSTFILLED,MOSTEMPTY}
    private int id;
    private Slot[] slots;
    private Status status;
    private InstanceType instanceType;
    private int emptySlots;
    private int occupiedSlots;

    public void load(String hostDetails) throws Exception {

        if (StringUtils.isBlank(hostDetails)) {
            throw new Exception("hostDetails is blank");
        }
        String[] data = StringUtils.split(hostDetails, ",");
        try {
            emptySlots= 0;
            occupiedSlots = 0;
            id = Integer.valueOf(data[0]);
            instanceType = InstanceType.valueOf(data[1]);
            int count = Integer.valueOf(data[2]);
            slots = new Slot[count];
            for (int a = 0; a < count; a++) {
                Slot slot = new Slot();
                int state = Integer.valueOf(data[a+3]);
                switch (state) {
                    case 0:
                        slot.setState(Slot.State.Empty);
                        emptySlots++;
                        break;
                    case 1:
                        slot.setState(Slot.State.Occupied);
                        occupiedSlots++;
                        break;
                    default:
                        slot.setState(Slot.State.NotSpecified);
                        break;
                }
                slots[a] = slot;
            }
            if(emptySlots == count) {
                status = Status.Empty;
            } else if(occupiedSlots == count){
                status = Status.FULL;
            } else if(emptySlots > 0 && emptySlots < occupiedSlots) {
                status = Status.MOSTFILLED;
            } else {
                status = Status.MOSTEMPTY;
            }
        } catch (Throwable e) {
            throw new Exception("failed to load {hostDetails = '" + hostDetails + "'} : " + e.getMessage(), e);
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Slot[] getSlots() {
        return slots;
    }

    public void setSlots(Slot[] slots) {
        this.slots = slots;
    }

    public InstanceType getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(InstanceType instanceType) {
        this.instanceType = instanceType;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int   getEmptySlots() {
        return emptySlots;
    }

    public void setOccupiedSlots(int occupiedSlots) {
        this.occupiedSlots = occupiedSlots;
    }
}
