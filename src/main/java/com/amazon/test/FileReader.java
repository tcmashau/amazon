package com.amazon.test;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * Created by caesarmashau on 2016/12/04.
 */
public class FileReader {

    private static final String fileName = "/FleetState.txt";

    public List<Host> getHosts() throws Exception {

        List<Host> hosts = new LinkedList<Host>();
        InputStream inputStream;
        try {
            inputStream = getClass().getResourceAsStream(fileName);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));
            String line = bufferedReader.readLine();
            while (line != null) {
                Host host = new Host();
                host.load(line);
                hosts.add(host);
                line = bufferedReader.readLine();
            }
        } catch (Throwable e) {
            throw new Exception("failed to load hosts {fileName='" + fileName + "'}" + e.getMessage(), e);
        }
        return hosts;

    }

    public Map<Host.Status, Map<Host.InstanceType, List<Host>>> getReport() throws Exception {

        Map<Host.Status, Map<Host.InstanceType, List<Host>>> report = new LinkedHashMap<Host.Status, Map<Host.InstanceType, List<Host>>>();
        List<Host> hosts = getHosts();
        for (Host host : hosts) {

            Host.Status status = host.getStatus();
            Map<Host.InstanceType, List<Host>> t = report.get(status);
            if (t == null) {
                t = new LinkedHashMap<Host.InstanceType, List<Host>>();
                List<Host> h = new LinkedList<Host>();
                h.add(host);
                t.put(host.getInstanceType(), h);
            } else {

                List<Host> h = t.get(host.getInstanceType());
                if (h == null) {
                    h = new LinkedList<Host>();
                    h.add(host);
                } else {
                    h.add(host);
                }
                t.put(host.getInstanceType(), h);
            }
            report.put(status, t);

        }
        return report;

    }

    public void printReport() throws Exception {

        StringBuffer sb = new StringBuffer();
        Map<Host.Status, Map<Host.InstanceType, List<Host>>> report = getReport();
        for (Host.Status status : report.keySet()) {
            sb.append(status.name() + ": ");
            Map<Host.InstanceType, List<Host>> js = report.get(status);
            for (Host.InstanceType instanceType : js.keySet()) {

                List<Host> hostList = js.get(instanceType);
                sb.append(instanceType.name() + "=" + hostList.size());
                if (status == Host.Status.MOSTFILLED) {
                    int emptySlotsCounter = 0;
                    for (Host host : hostList) {
                        emptySlotsCounter += host.getEmptySlots();
                    }
                    sb.append("," + emptySlotsCounter);
                }
                sb.append(";");
            }
            sb.append("\n");
        }
        String name = System.getProperty("user.home")+File.separator+"Statistics.txt";
        FileOutputStream outputStream = new FileOutputStream(name);
        IOUtils.write(sb.toString(),outputStream);
    }


}
