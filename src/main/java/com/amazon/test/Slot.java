package com.amazon.test;

/**
 * Created by caesarmashau on 2016/12/04.
 */
public class Slot {


    public enum State {Empty,Occupied,NotSpecified}
    private State state;


    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }


}
